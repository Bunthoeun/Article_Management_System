package org.kshrd.ams.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FileUploadConfiguration implements WebMvcConfigurer {
    String clientPath = "/image/";
    String serverPath = "C://Users/Sreng Channra/Desktop/UploadFile/";
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(clientPath + "**").addResourceLocations("file:" + serverPath);
    }
}
