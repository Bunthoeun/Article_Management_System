package org.kshrd.ams.controllers;

import org.kshrd.ams.services.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CategoryController {

    private CategoryService categoryService;
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("/cate")
    public String homePage(ModelMap map){
        map.addAttribute("categories", categoryService.findAll());
        System.out.println(categoryService.findAll());
        return "index";
    }
}
