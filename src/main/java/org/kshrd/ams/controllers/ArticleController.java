package org.kshrd.ams.controllers;

import org.kshrd.ams.models.Article;
import org.kshrd.ams.services.article.ArticleService;
import org.kshrd.ams.services.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Controller
public class ArticleController {

    private ArticleService articleService;
    private CategoryService categoryService;
    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("/")
    public String homePage(ModelMap map) {
        map.addAttribute("articles", articleService.findAll());
        return "index";
    }

    @GetMapping("/delete/{id}")
    public String deleteArticle(@PathVariable("id") int id) {
        articleService.deleteArticle(id);
        return "redirect:/";
    }

    @GetMapping("/view/{id}")
    public String viewArticle(@PathVariable("id") int id, ModelMap map) {
        Article article = articleService.findOne(id);
        map.addAttribute("article", article);
        return "view";
    }

    @GetMapping("/add")
    public String addArticle(ModelMap map) {
        map.addAttribute("article", new Article());
        map.addAttribute("categories", categoryService.findAll());
        map.addAttribute("formAdd", true);
        return "form";
    }

    @PostMapping("/add")
    public String saveArticle(@ModelAttribute Article article, ModelMap map, @RequestParam("file")MultipartFile file){
        article.setId(articleService.findAll().size()+1);
        article.setCreatedDate(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
        String serverPath = "C://users/Sreng Channra/Desktop/UploadFile/";
        String fileName = "";
        if(!file.isEmpty()){
            fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
            try {
                Files.copy(file.getInputStream(),Paths.get(serverPath, fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        article.setCategory(categoryService.findOne(article.getCategory().getId()));
        article.setThumbnail(fileName);
        System.out.println(fileName);
        articleService.addArticle(article);
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String updateArticle(@PathVariable("id") int id, ModelMap map) {
        Article article = articleService.findOne(id);
        map.addAttribute("article", article);
        map.addAttribute("categories", categoryService.findAll());
        map.addAttribute("formAdd", false);
        return "form";
    }

    @PostMapping("/update")
    public String saveUpdate(@ModelAttribute Article article) {
        articleService.updateArticle(article);
        return "redirect:/";
    }
}
