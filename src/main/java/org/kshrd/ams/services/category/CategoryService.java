package org.kshrd.ams.services.category;

import org.kshrd.ams.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category findOne(int id);
}
