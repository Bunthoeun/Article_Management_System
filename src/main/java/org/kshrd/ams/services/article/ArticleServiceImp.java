package org.kshrd.ams.services.article;

import org.kshrd.ams.models.Article;
import org.kshrd.ams.repositories.article.ArticleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    @Autowired
    private ArticleRepo articleRepo;
    @Override
    public List<Article> findAll() {
        return articleRepo.findAll();
    }

    @Override
    public void deleteArticle(int id) {
        articleRepo.deleteArticle(id);
    }

    @Override
    public Article findOne(int id) {
        return articleRepo.findOne(id);
    }

    @Override
    public void addArticle(Article article) {
        articleRepo.addArticle(article);
    }

    @Override
    public void updateArticle(Article article) {
        articleRepo.updateArticle(article);
    }
}
