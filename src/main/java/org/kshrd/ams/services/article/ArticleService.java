package org.kshrd.ams.services.article;

import org.kshrd.ams.models.Article;

import java.util.List;

public interface ArticleService {
    List<Article> findAll();
    void deleteArticle(int id);
    Article findOne(int id);
    void addArticle(Article article);
    void updateArticle(Article article);
}
