package org.kshrd.ams.repositories.article;

import org.kshrd.ams.models.Article;
import org.kshrd.ams.models.Category;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ArticleRepoImp implements ArticleRepo {

    private List<Article> articles = new ArrayList<>();

    public ArticleRepoImp() {
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        articles.add(new Article(1,"Getting Started spring", "Getting started with i18n", new Category(3, "Spring"), "key1.png", "Jonh Doe", date));
        articles.add(new Article(2,"Spring profile", "Change active state of profile", new Category(3, "Spring"), "picture.png", "Markton", date));
        articles.add(new Article(3,"Character or Markup", "Decide to use which one?", new Category(2, "Website"), "key2.png", "Jonh Doe", date));
        articles.add(new Article(4,"Who uses Unicode?", "Description who use unicode", new Category(1, "Java"), "key.png", "Ros Dara", date));
        articles.add(new Article(5,"Learning HTML", "Basic way to create website", new Category(2, "Website"), "picture.png", "Bunthoeun", date));
        articles.add(new Article(6,"Design Toturail", "UI Design toturail", new Category(2, "Website"), "key1.png", "Roseling", date));
        articles.add(new Article(7,"Speaking Korea", "Getting started with Korea", new Category(5, "Korea"), "picture.png", "Jonh Doe", date));
        articles.add(new Article(8,"Checking HTTP Header", "Ajax post request method", new Category(2, "Website"), "picture.png", "Itali company", date));
        articles.add(new Article(9,"Design ERD", "Database toturail Design", new Category(4, "Database"), "key2.png", "FRIDAY", date));
        articles.add(new Article(10,"Choosing a language tag", "Getting started with Website", new Category(2, "Website"), "key.png", "Jonh Doe", date));

    }
    @Override
    public List<Article> findAll() {
        return articles;
    }

    @Override
    public void deleteArticle(int id) {
        for(Article article: articles) {
            if(article.getId() == id) {
                articles.remove(article);
                return;
            }
        }
    }

    @Override
    public Article findOne(int id) {
        for (Article article: articles) {
            if(article.getId() == id) {
                return article;
            }
        }
        return null;
    }

    @Override
    public void addArticle(Article article) {
        articles.add(article);
    }

    @Override
    public void updateArticle(Article article) {
        for(Article art: articles) {
            if(art.getId() == article.getId()) {
                art.setTitle(article.getTitle());
                art.setDescription(article.getDescription());
                art.setCategory(article.getCategory());
                art.setAuthor(article.getAuthor());
                return;
            }
        }
    }
}
