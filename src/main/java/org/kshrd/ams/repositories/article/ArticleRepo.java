package org.kshrd.ams.repositories.article;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.kshrd.ams.models.Article;
import org.springframework.stereotype.Repository;

import java.util.List;
//@Repository
public interface ArticleRepo {
//    @Select("select a.id, a.title, a.description, a.thumbnail, a.author, a.create_date, a.category_id, c.name from tbl_articles a inner join tbl_categories c on a.categories_id = c.id order by a.id ASC")
    List<Article> findAll();
    void  deleteArticle(int id);
//    @Select("select a.id, a.title, a.description, a.thumbnail, a.author, a.create_date, a.category_id, c.name from tbl_articles a inner join tbl_categories c on a.categories_id = c.id where a.id = #{id}")
//    @Results({
//            @Result(property = "", column = "")
//    })
    Article findOne(int id);
//    @Insert("insert into tbl_articles(title, description, thumbnail, author, created_date, category_id) values(#{title}, #{description}, #{thumbnail}, #{author}, #{createdDate}, #{category.id}")
    void addArticle(Article article);
    void updateArticle(Article article);
}
