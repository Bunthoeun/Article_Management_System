package org.kshrd.ams.repositories.category;

import org.kshrd.ams.models.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryRepoImp implements CategoryRepo {

    private List<Category> categories = new ArrayList<>();

    public CategoryRepoImp() {
        categories.add(new Category(1, "Java"));
        categories.add(new Category(2, "Website"));
        categories.add(new Category(3, "Spring"));
        categories.add(new Category(4, "Database"));
        categories.add(new Category(5, "Korea"));
    }
    @Override
    public List<Category> findAll() {
        return categories;
    }

    @Override
    public Category findOne(int id) {
        for(Category category: categories) {
            if(category.getId() == id){
                return category;
            }
        }
        return null;
    }
}
