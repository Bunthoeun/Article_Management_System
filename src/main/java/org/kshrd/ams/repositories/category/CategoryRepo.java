package org.kshrd.ams.repositories.category;

import org.kshrd.ams.models.Category;

import java.util.List;

public interface CategoryRepo {
    List<Category> findAll();
    Category findOne(int id);
}
